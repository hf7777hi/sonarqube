#! /bin/bash
SONAR_DIR="/opt/sonar-scanner/"

cd `dirname $0`
PWD=`pwd`
PWD=`echo ${PWD} | sed "s#/c/#C:/#g"`
echo ${PWD}

sed -i.bak -e "s#sonar.sources=.#sonar.sources=${PWD}"/"${1}#g" sonar-scanner.properties

if [ `pwd | cut -b 1-2` = "C:" ] ; then
    echo "----- Windows -----"
    mv sonar-scanner.properties C:${SONAR_DIR}conf/
    echo "----- sonar-scanner.properties -----"
    cat ${SONAR_DIR}conf/sonar-scanner.properties
    echo "---------------"
    mv sonar-scanner.properties.bak sonar-scanner.properties
    C:${SONAR_DIR}bin/sonar-scanner.bat
else
    mv sonar-scanner.properties ${SONAR_DIR}conf/
    echo "----- sonar-scanner.properties -----"
    cat ${SONAR_DIR}conf/sonar-scanner.properties
    echo "---------------"
    mv sonar-scanner.properties.bak sonar-scanner.properties
    ${SONAR_DIR}bin/sonar-scanner
fi
