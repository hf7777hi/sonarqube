# docker
sudo groupadd docker
sudo gpasswd -a $USER docker
sudo systemctl start docker
sudo systemctl enable docker
# python3
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
echo 'export PATH="$HOME/.pyenv/bin:$PATH"' >> ~/.bash_profile
echo 'eval "$(pyenv init -)"' >> ~/.bash_profile
source ~/.bash_profile
pyenv install 3.6.6
pyenv global 3.6.6
pyenv rehash
pip install --upgrade pip
pip install docker-compose

