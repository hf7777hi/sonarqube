# Vagrant Setup
## version
* CentOS7.2
## setup
Vagrantfileがあるディレクトリで実施する
### 初回のみ
```
$ vagrant box add centos72 https://github.com/CommanderK5/packer-centos-template/releases/download/0.7.2/vagrant-centos-7.2.box
```
### VM起動 ログイン
```
$ vagrant up
$ vagrant ssh
```
### 初期セットアップ
時間結構かかります。
```
$ ./setup/setup.sh
```
# SonarQubeサーバ
## 起動
結構時間かかります。
```
$ cd ~/setup/sonarqube/
$ docker-compose up (-d)
```
## 終了
```
$ cd ~/setup/sonarqube/
$ docker-compose down
```
## アクセス
http://[VMのIP]:9000/

## 解析するプロジェクトを立てる
1. ログイン (初期アカウントは　admin/admin)
2. ログイン後以下のように設定する

![sonarqubeserver.png](https://bitbucket.org/repo/Ag7egML/images/1515941789-sonarqubeserver.png)

以下はどこかにメモる必要あり。

* token は、sonar-scanner.propertiesのsonar.loginに設定する
* project key はsonar.projectKeyに設定する 

# SonarQubeクライアント(解析)
## SonarQube Scanner DL
https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner
ここからDLする

展開は以下にする
/opt/sonar-scanner

## 設定
sonar-scanner.properties を解析したいプロジェクトに設置する
tokenとproject key等プロジェクト固有の必要情報を設定する。

## 解析
対象がtarget-project/src　の場合
```
$ ls
sonar-scanner.properties        sonar-scanner.sh                target-project
$ ls target-project/
README.md               composer.json           index.php               phpunit.xml.dist        src                     tmp                     webroot
bin                     config                  logs                    plugins                 tests                   vendor
```
```
$ ./sonar-scanner.sh test-sonar/src/
```
引数に解析する相対パスを渡して実行する。
SonarQubeサーバにアクセスすると結果が反映されている。

## プロジェクトを追加する
別のプロジェクトの解析を行いたい場合は、SonarQube上でプロジェクトを作成する必要があります。

以下のCreate new project

![addproject.png](https://bitbucket.org/repo/Ag7egML/images/73279509-addproject.png)

から、以下のように設定します。

![sonarqubeserver.png](https://bitbucket.org/repo/Ag7egML/images/1515941789-sonarqubeserver.png)

* token は、sonar-scanner.propertiesのsonar.loginに設定する
* project key はsonar.projectKeyに設定する 
